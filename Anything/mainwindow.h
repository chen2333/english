#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
//#include "observer.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_addButton_clicked();

    void on_searchButton1_clicked();
    void on_showtext1_textChanged();

    void on_showtext2_textChanged();

    void on_searchButton2_clicked();

private:
    Ui::MainWindow *ui;
};

using namespace std;
extern vector<vector<QString>> strings;
extern vector<QString> original;
extern vector<pair<QString, int>> ans;
extern bool flag[10005];

#endif // MAINWINDOW_H
