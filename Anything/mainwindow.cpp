#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "stdio.h"
#include <QDebug>
using namespace std;
QList <QColor> colorlist;
QString pubstr;
vector<vector<QString>> strings;
vector<QString> original;
vector<pair<QString, int>> ans;
bool flag[10005];
void stringToHtmlFilter(QString &str);
void stringToHtml(QString &str,QColor crl);
void convertHtml2(QString &str);
void convertHtml1(QString &str);

class Observer
{
public:
//    Observer(){}
//    ~Observer(){}
    QTextEdit *me;
    virtual void update(QTextEdit *) = 0;
};
class Subject
{
public:
//    Subject(){}
//    ~Subject(){}
    virtual void registerObserver(Observer *) = 0;
    virtual void removeObserver(Observer *) = 0;
    virtual void notifyObservers() = 0;
    QTextEdit *from;
};

class textclassa:public Observer{
public:

    void update(QTextEdit *from){
        qDebug() << "update" << endl;
        QTextCursor tc1 = from->textCursor();
        int tc1pos = tc1.position();
        QTextCursor tc2 = me->textCursor();
        tc2.setPosition(tc1pos);
        int len1 = from->toPlainText().length();
        int len2 = me->toPlainText().length();
        int lencha = len2 - len1;
        qDebug() << lencha << endl;
        for(int i=0; i<lencha; i++)
        {
            tc2.deleteChar();
        }
    }
private:
//    list<Observer *> m_lstObservers;
};

class textclassb:public Subject{
public:
    void registerObserver(Observer *o){
        m_lstObservers.push_back(o);
    }
    void removeObserver(Observer *o){
        if(m_lstObservers.size() > 0){
            m_lstObservers.remove(o);
        }
    }
    void notifyObservers(){
        foreach (Observer *o, m_lstObservers) {
            o->update(from);
        }
    }
private:
    list<Observer *> m_lstObservers;
};

//class textclass:public Observer,public Subject{
//public:
//    void registerObserver(Observer *o){
//        m_lstObservers.push_back(o);
//    }
//    void removeObserver(Observer *o){
//        if(m_lstObservers.size() > 0){
//            m_lstObservers.remove(o);
//        }
//    }
//    void notifyObservers(){
//        foreach (Observer *o, m_lstObservers) {
//            o->update(me);
//        }
//    }
//    void update(QTextEdit *from){
//        qDebug() << "update" << endl;
//        QTextCursor tc1 = from->textCursor();
//        int tc1pos = tc1.position();
//        QTextCursor tc2 = me->textCursor();
//        tc2.setPosition(tc1pos);
//        int len1 = from->toPlainText().length();
//        int len2 = me->toPlainText().length();
//        int lencha = len2 - len1;
//        qDebug() << lencha << endl;
//        for(int i=0; i<lencha; i++)
//        {
//            tc2.deleteChar();
//        }
//    }
//private:
//    list<Observer *> m_lstObservers;
//};

textclassa *editor11 = new textclassa;
textclassa *editor21 = new textclassa;
textclassb *editor12 = new textclassb;
textclassb *editor22 = new textclassb;

class Strategy
{
  public:
   virtual ~Strategy(){}
   virtual void AlgrithmInterface(QString &str) = 0;
};
class ConcreteStrategyA:public Strategy
{
  public:
    void AlgrithmInterface(QString &str);
};
class ConcreteStrategyB:public Strategy
{
  public:
    void AlgrithmInterface(QString &str);
};
void ConcreteStrategyA::AlgrithmInterface(QString &str)
{
    qDebug() <<"test ConcreteStrategyA.....";
    qDebug() << str;
    memset(flag, true, sizeof(flag));
    QString s;
    QString tmp = "";
    QString ans = "";
    for (int i = 0; i < (int)str.length(); i++)
    {
        if (str[i] != ' ' && str[i] != '\r' && str[i] != '\n')
            tmp += str[i];
        else
        {
            for (int i = 0; i < (int)strings.size(); i++)
            {
                bool res = false;
                for (auto & j : strings[i])
                    if (j == tmp)
                    {
                        res = true;
                        break;
                    }
                if (!res) flag[i] = false;
            }
            tmp = "";
        }
        if (str[i] != '\n') continue;
    }
    if (tmp != "")
        for (int i = 0; i < (int)strings.size(); i++)
            {
                bool res = false;
                for (auto & j : strings[i])
                    if (j == tmp)
                    {
                        res = true;
                        break;
                    }
                if (!res) flag[i] = false;
            }
    tmp = "";
    for (int i = 0; i < (int)strings.size(); i++)
        if (flag[i])
        {
            qDebug() << i;
            ans += original[i].mid(1, (int)original[i].size() - 2) + '\n';
        }
    str = ans;
}
void ConcreteStrategyB::AlgrithmInterface(QString &str)
{
    qDebug() <<"test ConcreteStrategyB....." << endl;
    qDebug() << str;
    memset(flag, true, sizeof(flag));
    QString s;
    QString tmp = "";
    QString ans = "";
    vector<vector<QString>> sstrings = strings;
    for (auto & i : sstrings)
        sort(i.begin(), i.end());
    for (int i = 0; i < (int)str.length(); i++)
    {
        if (str[i] != ' ' && str[i] != '\r' && str[i] != '\n')
            tmp += str[i];
        else
        {
            for (int i = 0; i < (int)sstrings.size(); i++)
            {
                auto x = lower_bound(sstrings[i].begin(), sstrings[i].end(), tmp);
                if (x == sstrings[i].end() || *x != tmp) flag[i] = false;
            }
            tmp = "";
        }
        if (str[i] != '\n') continue;
    }
    if (tmp != "")
        for (int i = 0; i < (int)sstrings.size(); i++)
        {
            auto x = lower_bound(sstrings[i].begin(), sstrings[i].end(), tmp);
            if (x == sstrings[i].end() || *x != tmp) flag[i] = false;
        }
    tmp = "";
    for (int i = 0; i < (int)strings.size(); i++)
        if (flag[i])
        {
            qDebug() << i;
            ans += original[i].mid(1, (int)original[i].size() - 2) + '\n';
        }
    str = ans;
}

class Context
{
  public:
    Context(Strategy* stg)
    {
      _stg = stg;
    }
    ~Context()
    {
      if (!_stg)
      delete _stg;
    }
    void ContextInterface(QString &constr)
    {

        _stg->AlgrithmInterface(constr);
    }
  private:
    Strategy* _stg;
};


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //防止系统操作激活
    ui->showtext1->blockSignals(true);
    ui->showtext2->blockSignals(true);

    colorlist.push_back(QColor(255,0,0));
    colorlist.push_back(QColor(0,255,0));
    colorlist.push_back(QColor(0,0,255));
    colorlist.push_back(QColor(255,255,0));
    colorlist.push_back(QColor(0,255,255));
    colorlist.push_back(QColor(255,0,255));
    colorlist.push_back(QColor(255,255,0));
    colorlist.push_back(QColor(0,0,0));

//    QTextDocument *xa = ui->showtext1->document();
//    QTextCursor cursor(xa);
//    qDebug() << cursor.blockNumber() << endl;
//    ui->showtext1->setStyleSheet("color: rgb(0, 0, 0)");
    QString str2 = ui->showtext2->toPlainText();
    ui->showtext1->setPlainText(str2);
//    QString str2 = ui->showtext2->toHtml();
//    convertHtml2(str2);
//    ui->showtext1->setHtml(str2);

    //解除禁止
    ui->showtext1->blockSignals(false);
    ui->showtext2->blockSignals(false);

    editor11->me = ui->showtext1;
    editor21->me = ui->showtext2;
    editor12->from = ui->showtext1;
    editor22->from = ui->showtext2;
    editor12->registerObserver(editor21); // 1的观察者（们）
    editor22->registerObserver(editor11); // 2的观察者（们）

}


MainWindow::~MainWindow()
{
    delete ui;
}
inline int getId(QChar ch)
{
    if (ch == '\0') return -1;
    if (ch == ' ') return 0;
    if ('a' <= ch && ch <= 'z') return (ch.toLatin1() - 'a') * 2 + 1;
    return (ch.toLatin1() - 'A' + 1) * 2;
}
bool cmp(const pair<QString, int> & c, const pair<QString,int> & d)
{
    QString a = c.first, b = d.first;
    for (int i = 0; i <= (int)max(a.length(), b.length()); i++)
    {
        if (getId(a[i]) != getId(b[i])) return getId(a[i]) < getId(b[i]);
    }
    return false;
}
void MainWindow::on_addButton_clicked()
{
    ui->showtext1->clear();
    ui->showtext2->clear();
    QString inputtext = ui->addtextEdit->toPlainText();
    //完成读入功能

    QStringList inpList = ui->addtextEdit->toPlainText().split("\n");
    for(int i = 0; i< inpList.size(); i++)
    {
        QString tmp = inpList[i];
        qDebug()<<"tmp ="<< tmp;
        original.push_back(" " + tmp + " ");
        QString now = "";
        vector<QString> ss;
        for (int j = 0; j < (int)tmp.length(); j++)
        {
            if (tmp[j] != ' ' &&  tmp[j] != '\r' &&  tmp[j] != '\n') now += tmp[j];
            else if (now != "")
            {
                ss.push_back(now);
                qDebug() << "now = " << now;
                now = "";
            }
        }
        if (now != "")
        {
            ss.push_back(now);
            qDebug() << "now = " << now;
            now = "";
        }
        strings.push_back(ss);
    }
    ans.clear();
    for (int i = 0; i < (int)strings.size(); i++)
    {
        int n = strings[i].size();
        for (int j = 0; j < n; j++)
        {
            QString tmp = "";
            for (int k = 0; k < n; k++)
            {
                tmp += strings[i][(j + k) % n];
                if (k != n - 1) tmp += " ";
            }
            ans.emplace_back(tmp, i);
        }
    }
    sort(ans.begin(), ans.end(), cmp);
    for (auto & i : ans)
    {
        qDebug() << i.first << " " << i.second;
        QColor color1 = colorlist.at(i.second);
        ui->showtext2->setTextColor(color1);
        ui->showtext2->append(i.first);
    }


    //......
    QString tempq = ui->showtext2->toPlainText();
    ui->showtext1->setText(tempq);
    pubstr = ui->showtext1->toPlainText();
    ui->addtextEdit->clear();
}

void stringToHtmlFilter(QString &str)
{
    //注意这几行代码的顺序不能乱，否则会造成多次替换
    str.replace("&","&amp;");
    str.replace(">","&gt;");
    str.replace("<","&lt;");
    str.replace("\"","&quot;");
    str.replace("\'","&#39;");
    str.replace(" ","&nbsp;");
    str.replace("\n","<br>");
    str.replace("\r","<br>");
}

void stringToHtml(QString &str,QColor crl)
{
     QByteArray array;
     array.append(crl.red());
     array.append(crl.green());
     array.append(crl.blue());
     QString strC(array.toHex());
    // str = QString("class = &quot;notranslate&quot;");
     str = QString("<span style=\" color:#%1;\">%2</span>").arg(strC).arg(str);
 }

void convertHtml2(QString &str)
{
    str.replace(QRegExp("<span style="), "<!--span style=");
    str.replace(QRegExp("00;\">"), "00;\"-->");
    str.replace(QRegExp("</span>"), "<!--/span-->");
}

void convertHtml1(QString &str)
{
    str.replace(QRegExp("<!--/span-->"), "</span>");
    str.replace(QRegExp("00;\"-->"), "00;\">");
    str.replace(QRegExp("<!--span style="), "<span style=");
}


void MainWindow::on_showtext1_textChanged()
{
    editor12->notifyObservers();
}

void MainWindow::on_showtext2_textChanged()
{
    editor22->notifyObservers();
}

void MainWindow::on_searchButton1_clicked()
{


    Strategy* ps = new ConcreteStrategyA();
    Context* pc = new Context(ps);
    QString strr = ui->searchtextEdit->toPlainText();
    pc->ContextInterface(strr);
    ui->searchresultTextEdit->setText(strr);
//    if (pc != NULL)
//    delete pc;
    //    QString texthtml2 = ui->showtext2->toHtml();
    //    qDebug() << texthtml2 << endl;
    //    QString newtexthtml = texthtml2;
    //    convertHtml2(newtexthtml);
    //    qDebug() << "erfes" << newtexthtml << endl;
    //    ui->showtext1->setHtml(newtexthtml);
    //    qDebug() << "hiw4" << ui->showtext1->toHtml() << endl;
    //    QString acsacsa = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'SimSun'; font-size:9.13433pt; font-weight:400; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" color:#c80000;\">sm iyioj ugtvyug.kuu</span></p>\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" color:#00c800;\">sdqm ugy</span></p>\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><!--span style=\" color:#00c800;\"-->sdqm ugy</span></p>\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" color:#00c800;\">sdqm ugy</span></p></body></html>";
    //    ui->showtext2->setHtml(acsacsa);
    //    QString adcsa = acsacsa.replace(QRegExp("<span style="), "<!--span style=");
    //    adcsa = acsacsa.replace(QRegExp("00;\">"), "00;\"-->");
    //    qDebug() << "hie6" << adcsa;
    //    ui->showtext2->setHtml(adcsa);
}

void MainWindow::on_searchButton2_clicked()
{
    Strategy* ps = new ConcreteStrategyB();
    Context* pc = new Context(ps);
    QString strr = ui->searchtextEdit->toPlainText();;
    pc->ContextInterface(strr);
    ui->searchresultTextEdit->setText(strr);
//    if (pc != NULL)
//    delete pc;
}
